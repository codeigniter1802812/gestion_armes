<?php
   class Produit extends CI_Controller{
    public function __construct(){
  		parent::__construct();
      $this->load->model('Produit_model');
  		if ($_SESSION['user_logged'] == FALSE) {
  			$this->session->set_flashdata("error","Conncecter aloha");
  			redirect("auth/login");
  		}
  	}
   	public function indexPro(){
   		//addcli
    		$this->form_validation->set_rules('designation','Designation','required',array('required' => 'Veuillez entrer la designation du produit'));
     		$this->form_validation->set_rules('qte','Quantite','required|numeric',array('required' => 'Veuillez entrer la quantite','numeric' =>'La quantite est invalide'));
         	$this->form_validation->set_rules('prix','Prix','required|numeric',array('required' => 'Veuillez entrer le prix','numeric' =>'Prix  invalide'));
         	//if form validation terue
         	if($this->form_validation->run() == TRUE){
         		
         		//add user in database;
         		$data = array(
                   'designation' => $_POST['designation'],
                   'qte' => $_POST['qte'],

                   'prix' => $_POST['prix'],
         		);
         		$this->db->insert('produit',$data);

         		$this->session->set_flashdata("success","produit ajouté avec succées");

         		redirect("produit/indexPro","refresh");
         	}
         	//liste
     	  $this->db->select('*');
      	$this->db->from('produit');
      	
      	$data["fetch_data"] = $this->db->get();
   	   	$data["sum"] = $this->Produit_model->get_count();
        $data["max"] = $this->Produit_model->get_maxprix();
        $data["min"] = $this->Produit_model->get_minprix();
        $data["sump"] = $this->Produit_model->get_sum_prix();
        $this->load->view('index_pro',$data);
   	}

    public function search(){
      //search
        $key = $this->input->post('key',true);
        $data['fetch_data'] = $this->Produit_model->set($key);
        $data["sum"] = $this->Produit_model->get_count();
        $data["max"] = $this->Produit_model->get_maxprix();
        $data["min"] = $this->Produit_model->get_minprix();
        $this->load->view('search_pro',$data);
    }
    public function edit($id){
    	$this->load->model('Produit_model');
    	$produit = $this->Produit_model->getproduit($id);
    	if (empty($produit)) {
    		$this->session->set_flashdata('error','produit non trouvé!');
    		redirect("produit/indexPro","refresh");
    	}
  		$this->form_validation->set_rules('designation','designation','required');
   		$this->form_validation->set_rules('qte','Quantite','required|numeric');
     	$this->form_validation->set_rules('prix','Prix','required|numeric');
     	//if form validation terue
     	if($this->form_validation->run() == TRUE){
     		$formArray['designation'] = $this->input->post('designation');
     		$formArray['qte'] = $this->input->post('qte');
     		$formArray['prix'] = $this->input->post('prix');
     		$this->Produit_model->update($id,$formArray);
     		$this->session->set_flashdata("success","produit modifier avec succées!");
     	   	$data['produit'] = $produit;
     		$this->load->view('addp',$data);
     	} else {
     		$data['produit'] = $produit;
     		$this->load->view('addp',$data);
     	}
    }
    public function delete($id){
    	$this->load->model('Produit_model');
    	$produit = $this->Produit_model->getproduit($id);
    	if (empty($produit)) {
    		$this->session->set_flashdata('error','produit non trouvé!');
    		redirect("produit/indexPro","refresh");
    	}
    	$this->Produit_model->delete($id);
    	$this->session->set_flashdata('success','produit efface!');
    	redirect("produit/indexPro","refresh");
    }
   
   }
?>