<?php
   class Auth extends CI_Controller {
   	  public function index(){
   	  		$this->form_validation->set_rules('username','Username','required',array('required' => 'Veuillez entrer votre nom utilisateur'));
   	  		$this->form_validation->set_rules('password','Password','required|min_length[6]',array('required' => 'Veuillez entrer votre mot de passse','min_length' => 'Mot de passse trop courte'));
            if($this->form_validation->run() == TRUE){
            	$username = $_POST['username'];
            	$password = md5($_POST['password']);

            	$this->db->select('*');
            	$this->db->from('users');
            	$this->db->where(array('username' => $username, 'password' => $password));
            	$query = $this->db->get();
            	$user = $query->row();

            	if($user){
                   //temporary message
            		$this->session->set_flashdata("success","Vous avez connecter");
            		//set session variables
            		$_SESSION['user_logged'] = TRUE;
            		$_SESSION['username'] = $user->username;
            		//redirect to profile page
            		redirect("user/profile","refresh");
            	} else {
            		$this->session->set_flashdata("error","Compte invalide!! ");
            		redirect("auth/index","refresh");
            	}
            }
   	  		$this->load->view('login');
   	  }
   	  public function register(){
         if (isset($_POST['register'])) {
         	$this->form_validation->set_rules('username','Username','required',array('required' => 'Veuillez entrer votre nom utilisateur'));
         	$this->form_validation->set_rules('email','Email','required',array('required' => 'Veuillez entrer votre email'));
         	$this->form_validation->set_rules('password','Password','required|min_length[6]',array('required' => 'Veuillez entrer votre mot de passse','min_length' => 'Mot de passse trop courte'));
         	$this->form_validation->set_rules('password2','Confirm password','required|min_length[6]|matches[password]',array('required' => 'Veuillez entrer votre confirmation mot de passse','min_length' => 'Confirmation invalide'));
         	$this->form_validation->set_rules('phone','Phone','required|min_length[10]',array('required' => 'Veuillez entrer votre numero telephone','min_length' =>'Le numero telephone incorrecte'));
         	//if form validation terue
         	if($this->form_validation->run() == TRUE){
         		
         		//add user in database;
         		$data = array(
                   'username' => $_POST['username'],
                   'email' => $_POST['email'],
                   'password' => md5($_POST['password']),
                   'gender' => $_POST['gender'],
                   'created_date' => date('Y-m-d'),
                   'phone' => $_POST['phone'],
         		);
         		$this->db->insert('users',$data);

         		$this->session->set_flashdata("success","Votre compte est inscrit. Vous pouvez s'authentifier maintenant");

         		redirect("auth/register","refresh");
         	}
         }
          //load view
   	  	$this->load->view('register');
   	  }

   	  public function logout(){
   	  	unset($_SESSION);
   	  	session_destroy();
   	  	redirect("auth/index","refresh");
   	  }
   }
?>