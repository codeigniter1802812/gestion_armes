<?php
  /**
   * 
   */
  class User extends CI_Controller
  {
  	public function __construct(){
  		parent::__construct();
      $this->load->model('Produit_model');
  		if ($_SESSION['user_logged'] == FALSE) {
  			$this->session->set_flashdata("error","Veuillez connecter!");
  			redirect("auth/index");
  		}
  	}
  	public function profile(){
        $this->db->select('*');
        $this->db->from('produit');
        
        $data["fetch_data"] = $this->db->get();
        if ($_SESSION['user_logged'] == FALSE) {
          $this->session->set_flashdata("error","Conncecter aloha");
          redirect("auth/login");
        }
        $data["count"] = $this->Produit_model->get_count();
        $data["sum_prix"] = $this->Produit_model->get_sum_prix();
        $data["min_prix"] = $this->Produit_model->get_minprix();
        $data["max_prix"] = $this->Produit_model->get_maxprix();
        $data["get_all"] = $this->Produit_model->get_all();
         $data["get_allp"] = $this->Produit_model->get_allp();
  		  $this->load->view('profile',$data);
  	}
  }
?>