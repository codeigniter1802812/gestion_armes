<?php
   class Commande extends CI_Controller{
    public function __construct(){
  		parent::__construct();
  		$this->load->model('Produit_model');
        $this->load->model('Commande_model');
  		if ($_SESSION['user_logged'] == FALSE) {
  			$this->session->set_flashdata("error","Conncecter aloha");
  			redirect("auth/login");
  		}
  	}
  	public function search(){
      //search
        $key = $this->input->post('key',true);
        $data['fetch_data'] = $this->Commande_model->set($key);
       
        $this->load->view('index_com',$data);
    }
    public function date_com(){
        $key = $this->input->post('date_com',true);
        $data['fetch_data'] = $this->Commande_model->set_date($key);
       
        $this->load->view('index_com',$data);
    }
    public function date_between(){
        $debut = $this->input->post('debut',true);
        $fin = $this->input->post('fin',true);
        $data['fetch_data'] = $this->Commande_model->set_between($debut,$fin);
       
        $this->load->view('index_com',$data);
    }
    public function indexCom(){
    	//client
      	$this->db->select('*');
      	$this->db->from('client');
      	$this->db->order_by('id','desc');
      	
      	$data["client"] = $this->db->get();
        //produit
      	$this->db->select('*');
      	$this->db->from('produit');
      
      	
      	$data["produit"] = $this->db->get();

    	$data['title'] = 'Commande';
    	$data['fetch_data'] = $this->Commande_model->get_com();
   	   	$data["sum"] = $this->Produit_model->get_count();
        $data["max"] = $this->Produit_model->get_maxprix();
        $data["min"] = $this->Produit_model->get_minprix();

        //addcom

		$this->form_validation->set_rules('qte','Quantite','required|numeric');
 		
     	//if form validation terue
     	if($this->form_validation->run() == TRUE){
     		
     		//add user in database;
     		$data = array(
               'id_client' => $_POST['id_client'],
               'id_produit' => $_POST['id_produit'],
               'qte' => $_POST['qte'],
			   'date_com' => date('Y-m-d'),
             
     		); 
        //maka ny valeur n quantite ny produit
     		    $idp = $_POST['id_produit'];
     		    $qteCom =   $_POST['qte'];
     		   
              $this->db->select('*');
            	$this->db->from('produit');
            	$this->db->where(array('id' => $idp));
            	$query = $this->db->get();
            	$prod = $query->row();

            	if($prod){
     			  $qteProd = $prod->qte;
     		       
				      if($qteProd >= $qteCom){
				      	$newQte = $qteProd - $qteCom;
				      	//insert commande
		     		    $this->db->insert('commande',$data);

		     		    //update produit
		     		    $formArray['qte'] = $newQte;
		     		    $this->db->where('id',$idp);
	  	            	$this->db->update('produit',$formArray);

		     		    $this->session->set_flashdata("success","commande ajouté avec succées");
	                    
		     		    redirect("commande/indexCom","refresh");
		     	      } else {
		     	      	$this->session->set_flashdata("error","Insuffisance");
		     		    redirect("commande/indexCom","refresh");
		     	      }
	     	  }

     		
     	}
    	 $this->load->view('index_com',$data);
    }
    public function detail($id){
	    $data['fetch_data'] = $this->Commande_model->getDetail($id);
	    $data['fetch_prod'] = $this->Commande_model->getDetailProd($id);
	    $this->load->view('detailcom',$data);
     	
    }
    // public function print($id){
	//     $data['fetch_data'] = $this->Commande_model->getDetail($id);
	//     $data['fetch_prod'] = $this->Commande_model->getDetailProd($id);
	//     $this->load->view('detailcom_print',$data);
     	
    // }
    public function edit($id){
    
    	$commande = $this->Commande_model->getCommande($id);
    	
    	if (empty($commande)) {
    		$this->session->set_flashdata('error','commande  non trouvé!');
    		redirect("commande/indexCom","refresh");
    	}
  	
   		$this->form_validation->set_rules('qte','Quantite','required|numeric');
     
     	//if form validation terue
     	if($this->form_validation->run() == TRUE){

     	    $idp =  $this->input->post('id_produit');
     		$qte = $this->input->post('qte');
     		//update produit
     		$this->db->select('*');
        	$this->db->from('produit');
        	$this->db->where(array('id' => $idp));
        	
        	$query = $this->db->get();
            
            $prod = $query->row();
            $qteProd = $prod->qte;
            $newQte = $qteProd + $qte;
     	    $formArray['qte'] = $newQte;
 		    $this->db->where('id',$idp);
            $this->db->update('produit',$formArray);
     		//delete com
     		$this->Commande_model->delete($id);
     		$this->session->set_flashdata("success","commande annuler avec succées!");
     	   	$data['commande'] = $commande;
     		$this->load->view('addcom',$data);
     		redirect("commande/indexCom","refresh");
     	} else {
     		$data['commande'] = $commande;
     		$this->load->view('addcom',$data);
     	}
    }
    public function delete($id){
    	
    	$commande = $this->Commande_model->getCommande($id);
    	if (empty($commande)) {
    		$this->session->set_flashdata('error','commadne non trouvé!');
    		redirect("commande/indexCom","refresh");
    	}
    	$this->Commande_model->delete($id);
    	$this->session->set_flashdata('success','commade efface!');
    	redirect("commande/indexCom","refresh");
    }
   
   }
?>