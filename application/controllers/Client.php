<?php
   class Client extends CI_Controller{
    public function __construct(){
  		parent::__construct();
      $this->load->model('Client_model');
  		if ($_SESSION['user_logged'] == FALSE) {
  			$this->session->set_flashdata("error","Conncecter aloha");
  			redirect("auth/index");
  		}
  	}

   	public function indexCli(){
   		//addcli
    		$this->form_validation->set_rules('nom','Nom','required|alpha',array('required' => 'Veuillez entrer votre nom','alpha' =>'Le nom ne contient pas des chiffres'));
     		$this->form_validation->set_rules('adresse','Adresse','required',array('required' => 'Veuillez entrer votre adresse'));
         	$this->form_validation->set_rules('tel','Telephone','required|numeric|min_length[10]',array('required' => 'Veuillez entrer votre numero telephone','numeric' =>'Le numero telephone invalide','min_length' =>'Le numero telephone incorrecte'));
         	//if form validation terue
         	if($this->form_validation->run() == TRUE){
         		
         		//add user in database;
         		$data = array(
                   'nom' => $_POST['nom'],
                   'adresse' => $_POST['adresse'],

                   'tel' => $_POST['tel'],
         		);
         		$this->db->insert('client',$data);

         		$this->session->set_flashdata("success","Client ajouté avec succées");

         		redirect("client/indexCli","refresh");
         	}
         	//liste
   	   	$this->db->select('*');
      	$this->db->from('client');
    	
      	$data["fetch_data"] = $this->db->get();
   	   	 $this->load->view('index_cli',$data);

         
   	}

   
    public function edit($id){
    	$this->load->model('Client_model');
    	$client = $this->Client_model->getClient($id);
    	if (empty($client)) {
    		$this->session->set_flashdata('error','Client non trouvé!');
    		redirect("client/indexCli","refresh");
    	}
		$this->form_validation->set_rules('nom','Nom','required|alpha');
 		$this->form_validation->set_rules('adresse','Adresse','required');
     	$this->form_validation->set_rules('tel','Telephone','required|min_length[10]');
     	//if form validation terue
     	if($this->form_validation->run() == TRUE){
     		$formArray['nom'] = $this->input->post('nom');
     		$formArray['adresse'] = $this->input->post('adresse');
     		$formArray['tel'] = $this->input->post('tel');
     		$this->Client_model->update($id,$formArray);
     		$this->session->set_flashdata("success","Cleint modifier avec succées!");
     	   	$data['client'] = $client;
     		$this->load->view('addc',$data);
     	} else {
     		$data['client'] = $client;
     		$this->load->view('addc',$data);
     	}
    }
    public function delete($id){
    	$this->load->model('Client_model');
    	$client = $this->Client_model->getClient($id);
    	if (empty($client)) {
    		$this->session->set_flashdata('error','Client non trouvé!');
    		redirect("client/indexCli","refresh");
    	}
    	$this->Client_model->delete($id);
    	$this->session->set_flashdata('success','Client efface!');
    	redirect("client/indexCli","refresh");
    }
     public function search(){
      //search
        $key = $this->input->post('key',true);
        $data['fetch_data'] = $this->Client_model->set($key);
       
        $this->load->view('index_cli',$data);
    }
   }
?>