<?php 
  /**
   * 
   */
  class Commande_model extends CI_Model
  {
    public function __construct(){
      parent::__construct();
      
    } 
    public function set($key){
      $this->db->select(['com.id','com.id_client','cli.nom','prod.designation','prod.prix','com.qte','com.date_com']);
      $this->db->from('commande com');
      $this->db->join('client cli','cli.id = com.id_client');
      $this->db->join('produit prod','prod.id = com.id_produit');
      $this->db->like('cli.id',$key);
      $this->db->or_like('cli.nom',$key);
      $this->db->or_like('prod.designation',$key);
      $this->db->or_like('prod.prix',$key);
      $this->db->or_like('com.id',$key);
      return $this->db->get();
    }
    public function set_date($key){
      $this->db->select(['com.id','com.id_client','cli.nom','prod.designation','prod.prix','com.qte','com.date_com']);
      $this->db->from('commande com');
      $this->db->join('client cli','cli.id = com.id_client');
      $this->db->join('produit prod','prod.id = com.id_produit');
      $this->db->like('com.date_com',$key);
     
      return $this->db->get();
    }
    public function set_between($debut,$fin){
       $this->db->select(['com.id','com.id_client','cli.nom','prod.designation','prod.prix','com.qte','com.date_com']);
       $this->db->from('commande com');
       $this->db->join('client cli','cli.id = com.id_client');
       $this->db->join('produit prod','prod.id = com.id_produit');
       $this->db->where('com.date_com >=',$debut);
       $this->db->where('com.date_com <=',$fin);
       $this->db->order_by('com.id');
       $commande = $this->db->get();
       return $commande;
    }
    public function get_com(){
      $this->db->select(['com.id','com.id_client','cli.nom','prod.designation','prod.prix','com.qte','com.date_com']);
      $this->db->from('commande com');
      $this->db->join('client cli','cli.id = com.id_client');
      $this->db->join('produit prod','prod.id = com.id_produit');
      $this->db->order_by('com.id');
      return  $this->db->get();
    }

    public function getCommande($id){
       $this->db->where('id',$id);
       $commande = $this->db->get('commande')->row_array();
       return $commande;
     }
    public function getDetail($id){
        $this->db->select(['cli.id','cli.nom','cli.adresse','cli.tel']);
        $this->db->from('client cli');
      
         $this->db->where('cli.id',$id);
        return  $this->db->get();
     }
    
    public function getDetailProd($id){
      $this->db->select(['prod.id','prod.designation','prod.prix','com.qte','com.id_client']);
      $this->db->from('commande com');
      $this->db->join('client cli','cli.id = com.id_client');
      $this->db->join('produit prod','prod.id = com.id_produit');
      $this->db->where('com.id_client',$id);
       $this->db->order_by('prod.id','asc');
      return  $this->db->get();
    }
     
    public function delete($id){
       $this->db->where('id',$id);
       $this->db->delete('commande');
     }
     public function update($id,$formArray){
       $this->db->where('id',$id);
       $this->db->update('produit',$formArray);
     }
  }
?>