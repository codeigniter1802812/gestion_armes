<?php 
  /**
   * 
   */
  class Client_model extends CI_Model
  {
  	 public function getClient($id){
  	 	 $this->db->where('id',$id);
  	 	 $client = $this->db->get('client')->row_array();
  	 	 return $client;
  	 }
  	 public function update($id,$formArray){
  	 	 $this->db->where('id',$id);
  	 	 $this->db->update('client',$formArray);
  	 }
  	 public function delete($id){
  	 	 $this->db->where('id',$id);
  	 	 $this->db->delete('client');
  	 }
    public function set($key){
      $this->db->select('*');
      $this->db->like('id',$key);
      $this->db->or_like('nom',$key);
      $this->db->or_like('tel',$key);
      $this->db->or_like('adresse',$key);
      return $this->db->get('client');
    }
  }
?>