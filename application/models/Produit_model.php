<?php 
  /**
   * 
   */
  class Produit_model extends CI_Model
  {
    public function __construct(){
      parent::__construct();
      
    } 
    public function get_count(){
      $sql = "SELECT count(id) as count_id FROM produit";
      $result = $this->db->query($sql);
      return $result->row()->count_id;
    }
     public function get_sum_prix(){
      $sql = "SELECT sum(prix) as count_id FROM produit";
      $result = $this->db->query($sql);
      return $result->row()->count_id;
    }
    public function get_maxprix(){
      $sql = "SELECT max(prix) as max_prix FROM produit";
      $result = $this->db->query($sql);
      return $result->row()->max_prix;
    }
    public function get_minprix(){
      $sql = "SELECT min(prix) as min_prix FROM produit";
      $result = $this->db->query($sql);
      return $result->row()->min_prix;
    }
    public function set($key){
      $this->db->select('*');
      $this->db->like('id',$key);
      $this->db->or_like('designation',$key);
      $this->db->or_like('qte',$key);
      $this->db->or_like('prix',$key);
      return $this->db->get('produit');
    }
  	 public function getProduit($id){
  	 	 $this->db->where('id',$id);
  	 	 $produit = $this->db->get('produit')->row_array();
  	 	 return $produit;
  	 }
  	 public function update($id,$formArray){
  	 	 $this->db->where('id',$id);
  	 	 $this->db->update('produit',$formArray);
  	 }
  	 public function delete($id){
  	 	 $this->db->where('id',$id);
  	 	 $this->db->delete('produit');
  	 }
     public function fetch_data($query){
       
     
       $this->db->select("*");
       $this->db->from("produit");
       if ($query !='') {
         $this->db->like('designation',$query);
       }
       return $this->db->get();
    } 
     public function get_all(){
        $this->db->select('*');
        $this->db->from('produit');
        $this->db->limit(4);
       return $this->db->get()->result();
     }
    public function get_allp(){
        $this->db->select('*');
        $this->db->from('produit');
        $this->db->order_by('id','desc');
        $this->db->limit(4);
       return $this->db->get()->result();
     }

  }
?>