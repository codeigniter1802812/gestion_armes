<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Gestion armes | produit</title>

  <!-- Font Awesome Icons -->
   <link rel="stylesheet" href="<?php echo base_url()."assets/"; ?>plugins/fontawesome-free/css/all.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo base_url()."assets/"; ?>plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url()."assets/"; ?>dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?php echo base_url();?>user/profile" class="nav-link">Accueil</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?php echo base_url();?>client/indexCli" class="nav-link ">Client</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?php echo base_url();?>produit/indexPro" class="nav-link active">Produit</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?php echo base_url();?>commande/indexCom" class="nav-link">Commande</a>
      </li>
    </ul>

   

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
     
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i
            class="fas fa-th-large"></i></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-success elevation-4">
    <!-- Brand Logo -->
      <a href="#" class="brand-link" style="background-color: #454c41;">
        <img src="<?php echo base_url()."assets/"; ?>dist/img/CHAINE.jpg" alt="Logo" class="brand-image img-circle elevation-4">
        <span class="brand-text font-weight-light"><?php echo $_SESSION['username'];?></span>
      </a>

    <!-- Sidebar -->
    <div class="sidebar" style="background-color: #150f08;">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
         
        </div>
        <div class="info">
         
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Menu
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
            <li class="nav-item">
                <a href="<?php echo base_url();?>client/indexCli" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Client</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url();?>produit/indexPro" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Produit</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url();?>commande/indexCom" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Commande</p>
                </a>
              </li>
            </ul>
          </li>
          
         
       
        
          <li class="nav-header">Information</li>
          <li class="nav-item">
            <a href="<?php echo base_url();?>auth/logout"" class="nav-link">
              <i class="nav-icon far fa-circle text-danger"></i>
              <p class="text">Deconnecter</p>
            </a>
          </li>
         
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Information sur les produits</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Accueil</a></li>
              <li class="breadcrumb-item active">Produit</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box">
              <span class="info-box-icon bg-default elevation-1"><i class="fas fa-shopping-cart"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Prix total</span>
                <span class="info-box-number">
                  <?php echo $sump; ?> Ariary
                  <small></small>
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-default elevation-1"><i class="fas fa-shopping-cart"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Prix minimal</span>
                <span class="info-box-number"><?php echo $min; ?> Ariary</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->

          <!-- fix for small devices only -->
          <div class="clearfix hidden-md-up"></div>

          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-default elevation-1"><i class="fas fa-shopping-cart"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Prix maximal</span>
                <span class="info-box-number"><?php echo $max; ?> Ariary</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-default elevation-1"><i class="fas fa-shopping-cart"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Produit total</span>
                <span class="info-box-number"><?php echo $sum; ?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

         <div class="row">
          <!-- Left col -->
          <div class="col-md-12">

             <?php if(isset($_SESSION['error'])){ ?> 
             <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-check"></i> Info! <?php echo $_SESSION['serror']; ?></h5>
                 
             </div>
             <?php   } ?>
          </div>
        </div>
         <div class="row">
          <!-- Left col -->
          <div class="col-md-12">

             <?php if(isset($_SESSION['success'])){ ?> 
             <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-check"></i> Info! <?php echo $_SESSION['success']; ?></h5>
                 
             </div>
             <?php   } ?>
          </div>
        </div>
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <div class="col-md-8">

          
            <!-- TABLE: LATEST ORDERS -->
            <div class="card">
              <div class="card-header border-transparent">
                <h3 class="card-title">Liste des produits</h3>


                <div class="card-tools">
                  <?php echo form_open('produit/search');?>
                       <div class="input-group input-group-sm">
                       <input class="form-control form-control-navbar" placeholder="Chercher" aria-label="Search" name="key" id="key">
                        <div class="input-group-append">
                          <button class="btn btn-navbar" type="submit" name="submit">
                            <i class="fas fa-search"></i>
                          </button>
                        </div>
                    </div>
                  <?php echo form_close();?>
                 
                 
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <div class="table-responsive">
                  <table class="table m-0">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>Designation</th>
                      <th>Quantite</th>
                      <th>Prix</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php 
                    if($fetch_data->num_rows() > 0)
                    {	
                    	foreach ($fetch_data->result() as $row) {
                     ?> 		
                    	
	                    <tr>
	                      <td><?php echo $row->id; ?></td>
	                      <td><?php echo $row->designation; ?></td>   
	                      <td><?php echo $row->qte; ?></td>  
	                      <td><?php echo $row->prix; ?></td>  
                        <td><a href="<?php echo base_url().'produit/edit/'.$row->id?>" class="btn btn-warning btn-sm"><i class="far fa-edit"></i>Modifier</a>
                        <a href="javascript:void(0);" onclick="deteleProduit(<?php echo $row->id; ?>)" class="btn btn-danger btn-sm"><i class="far fa-trash-alt"></i>Effacer</a></td>     
	                    </tr>
                    <?php
                      }
                    }
                    else 
                    {
                    ?>
                     <tr>
                     	<td>Aucun donnée trouver!!</td>
                     </tr>	
                    <?php } ?>
                    
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.card-body -->
             
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          
          <div class="col-md-4">
            <!-- Info Boxes Style 2 -->
             <div class="card card-default">
              <div class="card-header">
                <h3 class="card-title">Ajout nouveau produit</h3>
                
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>

              </div>
            <form action="" method="POST" name="myForm" onsubmit="return validateForm()"><br>
               
              <div class="card-body">
                <div class="form-group">
                  <label for="designation" class="text<?php echo (form_error('designation') != "") ? '-danger' : ''; ?>">Designation</label>
                  <input type="text" id="designation" name="designation" class="form-control <?php echo (form_error('designation') != "") ? 'is-invalid' : ''; ?>" value="<?php echo set_value('designation'); ?>">
                  <span class="text-danger"><?php echo form_error('designation');?></span>
                </div>
                <div class="form-group">
                  <label for="qte" class="text<?php echo (form_error('qte') != "") ? '-danger' : ''; ?>">Quantite</label>
                  <input type="text" id="qte" name="qte" class="form-control <?php echo (form_error('qte') != "") ? 'is-invalid' : ''; ?>" value="<?php echo set_value('qte'); ?>">
                  <span class="text-danger"><?php echo form_error('qte');?></span>
                </div>
                <div class="form-group">
                  <label for="prix" class="text<?php echo (form_error('prix') != "") ? '-danger' : ''; ?>">Prix</label>
                  <input type="text" id="prix" value="<?php echo set_value('prix'); ?>" name="prix" class="form-control <?php echo (form_error('prix') != "") ? 'is-invalid' : ''; ?>">
                  <span class="text-danger"><?php echo form_error('prix');?></span>
                </div>
               
              </div>
              <div class="card-footer clearfix">
                  <input type="submit" value="Enregistrer" class="btn btn-success float-right">
              </div>
            </div>
           </form>
         
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
    
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
     <script>
            function validateForm() {
            
                var nomp = document.forms["myForm"]["designation"].value;
                var qte = document.forms["myForm"]["qte"].value;
                var prix = document.forms["myForm"]["prix"].value;
                
              
                var maj = nomp.toUpperCase();
                document.forms["myForm"]["designation"].value = maj;

                
                //adresse
               
                if (isNaN(qte) == true) {
                    alert("Le quantite doit etre en chriffre");
                    return false;

                } 
                 //prix
                 if (isNaN(prix) == true) {
                    alert("Le prix doit etre en chriffre");
                    return false;

                } 
            }

        </script>
  <script type="text/javascript">
    function deteleProduit(id){
       if (confirm("Voulez vouz supprimer?")) {
        window.location.href='<?php echo base_url().'produit/delete/';?>'+id;
       }
    }
  </script>
<?php $this->load->view('footer');?>

