<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Gestion | Vente</title>
    <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url()."assets/";?>plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url()."assets/";?>plugins/daterangepicker/daterangepicker.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?php echo base_url()."assets/";?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?php echo base_url()."assets/";?>plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="<?php echo base_url()."assets/";?>plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url()."assets/";?>plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="<?php echo base_url()."assets/";?>plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <!-- Bootstrap4 Duallistbox -->
  <link rel="stylesheet" href="<?php echo base_url()."assets/";?>plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url()."assets/";?>dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?php echo base_url();?>user/profile" class="nav-link">Accueil</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?php echo base_url();?>client/indexCli" class="nav-link ">Client</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?php echo base_url();?>produit/indexPro" class="nav-link">Produit</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?php echo base_url();?>commande/indexCom" class="nav-link active">Commande</a>
      </li>
    </ul>

   

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
     
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i
            class="fas fa-th-large"></i></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-success elevation-4">
    <!-- Brand Logo -->
      <a href="#" class="brand-link" style="background-color: #454c41;">
        <img src="<?php echo base_url()."assets/"; ?>dist/img/CHAINE.jpg" alt="Logo" class="brand-image img-circle elevation-4">
        <span class="brand-text font-weight-light"><?php echo $_SESSION['username'];?></span>
      </a>

    <!-- Sidebar -->
    <div class="sidebar"  style="background-color: #150f08;">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
        
        </div>
        <div class="info">
         
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Menu
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
            <li class="nav-item">
                <a href="<?php echo base_url();?>client/indexCli" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Client</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url();?>produit/indexPro" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Produit</p>
                </a>
              </li>
              <li class="nav-item ">
                <a href="<?php echo base_url();?>commande/indexCom" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Commande</p>
                </a>
              </li>
            </ul>
          </li>
          
         
       
        
          <li class="nav-header">Information</li>
          <li class="nav-item">
            <a href="<?php echo base_url();?>auth/logout"" class="nav-link">
              <i class="nav-icon far fa-circle text-danger"></i>
              <p class="text">Deconnecter</p>
            </a>
          </li>
         
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Information sur les commandes</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Accueil</a></li>
              <li class="breadcrumb-item active">commande</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       

         <div class="row">
          <!-- Left col -->
          <div class="col-md-12">

             <?php if(isset($_SESSION['error'])){ ?> 
             <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-check"></i> Info! <?php echo $_SESSION['error']; ?></h5>
                 
             </div>
             <?php   } ?>
          </div>
        </div>
         <div class="row">
          <!-- Left col -->
          <div class="col-md-12">

             <?php if(isset($_SESSION['success'])){ ?> 
             <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-check"></i> Info! <?php echo $_SESSION['success']; ?></h5>
                 
             </div>
             <?php   } ?>
          </div>
        </div>
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <div class="col-md-9">

          
            <!-- TABLE: LATEST ORDERS -->
            <div class="card">
              <div class="card-header border-transparent">
                <h3 class="card-title"><i class="fas fa-user"></i> Liste des commandes</h3>


                <div class="card-tools">
                  <?php echo form_open('commande/search');?>
                       <div class="input-group input-group-sm">
                       <input class="form-control form-control-navbar" placeholder="Chercher" aria-label="Search" name="key" id="key">
                        <div class="input-group-append">
                          <button class="btn btn-navbar" type="submit" name="submit">
                            <i class="fas fa-search"></i>
                          </button>
                        </div>
                    </div>
                  <?php echo form_close();?>
                 
                 
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <div class="table-responsive">
                  <table class="table m-0">
                    <thead>
                    <tr>
                    
                      <th>Client</th>
                      <th>Produit</th>
                      <th>Prix </th>
                      <th>Quantite</th>
                      <th>Sous total</th>
                      <th>Date</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php 
                    if($fetch_data->num_rows() > 0)
                    {	
                    	foreach ($fetch_data->result() as $row) {
                     ?> 		
                    	
	                    <tr>
	                    
	                      <td><?php echo $row->nom; ?></td>   
	                      <td><?php echo $row->designation; ?></td>  
	                      <td><?php echo $row->prix; ?></td> 
                        <td><?php echo $row->qte; ?></td> 
                        <td><?php echo $row->prix * $row->qte; ?></td> 
                        <td><?php echo $row->date_com; ?></td> 
                        <td>
                          <a href="<?php echo base_url().'commande/edit/'.$row->id?>" class="btn btn-warning btn-xs" ><i class="far fa-edit"></i>Annuler</a>
                        </td>  
                        <td>
                          <a href="<?php echo base_url().'commande/detail/'.$row->id_client;?>"  class="btn btn-default btn-xs"><i class="fas fa-info"></i>  Detail</a>
                        </td>  
	                    </tr>
                    <?php
                      }
                    }
                    else 
                    {
                    ?>
                      <div class="alert alert-danger">Aucun resultat trouvé !!</div>
                    <?php } ?>
                    
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.card-body -->
              <!-- /.card-body -->
              <div class="card-footer clearfix">
              
                <button type="button" class="btn btn-sm btn-info float-left" data-toggle="modal" data-target="#modal-default">
                  Nouveau commande
                </button>
                <a href="<?php echo base_url();?>commande/indexCom" class="btn btn-sm btn-secondary float-right">Toutes les listes</a>
              </div>
              <!-- /.card-footer -->
              <!-- /.card-footer -->
            </div>
            
          </div>
          <!-- /.col -->

          <div class="col-md-3">
            <!-- Info Boxes Style 2 -->
             <div class="card card-default">
              <div class="card-header">
                <h6 ><i class="fas fa-info"></i> Requete sur les commandes</h6>
              </div>

              <div class="card-body">
                <div class="card-tools">
                  <?php echo form_open('commande/date_com');?>
                       <label>Date de commande:</label>
                       <div class="input-group input-group-sm">

                       <input type="date" class="form-control form-control-navbar" placeholder="Entrer date de commande" aria-label="Search" name="date_com" id="date_com" value="<?php echo set_value('date_com'); ?>">
                        <div class="input-group-append">
                          <button class="btn btn-navbar" type="submit" name="submit">
                            <i class="fas fa-play"></i>
                          </button>
                        </div>
                    </div>
                  <?php echo form_close();?>
                 
                 
                </div><hr>
                <div class="card-tools">
                  <?php echo form_open('commande/date_between');?>
                       <label>Commande entre:</label>
                       <div class="input-group input-group-sm">

                         <input type="date" class="form-control form-control-navbar" placeholder="Entrer date de commande" aria-label="Search" name="debut" id="debut" value="<?php echo set_value('date_com'); ?>" style=" width: 47%; position: absolute;">
                        
                         <input type="date" class="form-control form-control-navbar" placeholder="Entrer date de commande" aria-label="Search" name="fin" id="fin" value="<?php echo set_value('date_com'); ?>" style=" width: 47%; position: absolute;left: 50%;">
                       
                       </div>
                       
                
                 
                 
                      </div><br><br>
                       <div class="input-group-append">
                          <button class="btn btn-outline-success" type="submit" name="submit">
                            Valider
                          </button>
                       </div>
                  <?php echo form_close();?>
              </div>
             
           </div>
           
         
          </div>
          
        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
    
  </div>
  <!-- /.content-wrapper -->
  <div class="modal fade" id="modal-default">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Ajout commande</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form action="" method="POST"  name="myForm" onsubmit="return validateForm()">
            <div class="modal-body">

                <div class="form-group">
                  <label for="nom">Nom client</label>
                  <select name="id_client" class="form-control select2 select2-danger" data-dropdown-css-class="select2-default" style="width: 100%;">
                     <?php 
                     foreach ($client->result() as $row) {
                     ?>     
                        <option value="<?php echo $row->id; ?>"><?php echo $row->nom; ?></option>
                     <?php
                        }
                     ?> 
                  </select>
                </div>
                <div class="form-group">
                  <label for="nom">Produit commandé</label>
                  <select name="id_produit" class="form-control select2 select2-danger" data-dropdown-css-class="select2-default" style="width: 100%;">
                     <?php 
                     foreach ($produit->result() as $row) {
                     ?>     
                        <option value="<?php echo $row->id; ?>"><?php echo $row->designation; ?></option>
                     <?php
                        }
                     ?> 
                  </select>
                </div>
                <div class="form-group">
                  <label for="qte">Quantité commandé</label>
                  <input type="text" id="qte" value="<?php echo set_value('qte'); ?>" name="qte" class="form-control <?php echo (form_error('qte') != "") ? 'is-invalid' : ''; ?>">
                  <span class="text-danger"><?php echo form_error('qte');?></span>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
              <button type="submit"  class="btn btn-primary">Enregistrer</button>
            </div> 
          </form>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
  </div>
      <!-- /.modal -->
    <!-- /.content-wrapper -->
  
      <!-- /.modal-dialog -->
  </div>
      <!-- /.modal -->    
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- Page script -->
 <script>
            function validateForm() {
                var qte = document.forms["myForm"]["qte"].value;
               
                if (qte == "") {
                    alert("Veuillez entrer la quantite commandé");
                    return false;
                } else if (isNaN(qte) == true) {
                    alert("Le quantite doit etre en chriffre");
                    return false;

                } else if (qte <= 0){
                   alert("Le quantite negatif, veullez entrer quantite superieur à 0");
                    return false;
                }
               
            }

        </script>

  <!-- /.control-sidebar -->
  
<footer class="main-footer">
    <strong>Copyright &copy; 2020 <a href="http://adminlte.io">Gestion vente</a>.</strong>
    Tous droit reservé.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.0.1
    </div>
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="<?php echo base_url()."assets/";?>plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap 4 -->
<script src="<?php echo base_url()."assets/";?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url()."assets/";?>plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="<?php echo base_url()."assets/";?>plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url()."assets/";?>plugins/moment/moment.min.js"></script>
<script src="<?php echo base_url()."assets/";?>plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
<!-- date-range-picker -->
<script src="<?php echo base_url()."assets/";?>plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->
<script src="<?php echo base_url()."assets/";?>plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?php echo base_url()."assets/";?>plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Bootstrap Switch -->
<script src="<?php echo base_url()."assets/";?>plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url()."assets/";?>dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url()."assets/";?>dist/js/demo.js"></script>
<!-- Page script -->
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

  })
</script>
</body>
</html>


