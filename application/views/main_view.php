<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 3 | General Form Elements</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url()."assets/"; ?>plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url()."assets/"; ?>https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="<?php echo base_url()."assets/"; ?>plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url()."assets/"; ?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="<?php echo base_url()."assets/"; ?>plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url()."assets/"; ?>dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo base_url()."assets/"; ?>plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url()."assets/"; ?>plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="<?php echo base_url()."assets/"; ?>plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="<?php echo base_url()."assets/"; ?>https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body>
    <div class="container">
    	<br/><br/><br/>
    	   <h3 align="center">Insert data</h3><br />
    	   <form method="post" action="<?php echo base_url()?>main/form_validation">
          <?php       
    	   	if($this->uri->segment(2) == "inserted"){
    	   		echo '<p class="text-success">inserted</p>';
    	   	}
    	     ?> 		
    	   	  <div class="form-group">
    	   	  	<label>Entrer nom</label>
    	   	  	<input type="text" name="first_name" class="form-control">
    	   	  	<span class="text-danger"><?php echo form_error('first_name');?></span>
    	   	  </div>
    	   	   <div class="form-group">
    	   	  	<label>Entrer prenom</label>
    	   	  	<input type="text" name="last_name" class="form-control">
    	   	  	<span class="text-danger"><?php echo form_error('last_name');?></span>
    	   	  </div>
    	   	  <div class="form-group">
    	   	  
    	   	  	<input type="submit" name="insert" value="Insert" class="btn btn-info">
    	   	  </div>
    	   </form>
    	   <br><br>
    	   <h3>Fetch Data from Table using Codeigniter</h3><br>
    	   <div class="table-response">
    	   	<table class="table table-bordered">
    	   		<tr>
    	   			<th>ID</th>
    	   			<th>First Name</th>
    	   			<th>Last Name</th>
    	   			<th>Delete</th>
    	   		</tr>
    	   		<?php 
                if ($fetch_data->num_rows() > 0) {
                	foreach ($fetch_data->result() as $row) 
                	{
                ?>
	                    <tr>
	                    	<td><?php echo $row->id; ?></td>
	                    	<td><?php echo $row->first_name; ?></td>
	                    	<td><?php echo $row->last_name; ?></td>
	                    	<td><a href="" class="delete_data" id="<?php echo $row->id;?>"> Delete</a></td>
	                    </tr>
                <?php   
                	}

                } else 
                {
                ?>	
                        <tr>
                        	<td colspan="4">No data found</td>
                        </tr>
                <?php } ?>
    	   		
    	   	</table>
    	   	
    	   </div>
    	   <script>
    	   	 $(document).ready(function(){
    	   	 	$('.delete_data').click(function(){
    	   	 		var id = $(this).attr("id");
    	   	 		if (confirm("Are you sure to delete?")) {
    	   	 			window.location="<?php echo base_url();?>main/delete_data/"+id;
    	   	 		} else {
    	   	 			return false;
    	   	 		}
    	   	 	})
    	   	 });
    	   </script>
    </div>
</body>
</html>