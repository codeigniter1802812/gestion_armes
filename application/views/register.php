<!DOCTYPE html>
<html>
<head>
   <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Gestion vente | login</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url()."assets/"; ?>plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="<?php echo base_url()."assets/"; ?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url()."assets/"; ?>dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition register-page">
  <div class="register-box">
    <div class="register-logo">
      <a href="../../index2.html"><b>Gestion</b>Arme</a>
    </div>  
        <?php if(isset($_SESSION['success'])){ ?> 
    	   		<div class="alert alert-success"><?php echo $_SESSION['success']; ?></div>
    	<?php   } ?> 
     <div class="card">
      <div class="card-body register-card-body">
        <p class="login-box-msg">Inscrivez-vous</p>
      	  <form action="" method="POST">	     	
            <div class="input-group mb-3">
                <input  type="text" name="username" class="form-control  <?php echo (form_error('username') != "") ? 'is-invalid' : ''; ?>" placeholder="Nom d'utilisateur">
                  <div class="input-group-append">
	                <div class="input-group-text">
	                  <span class="fas fa-user"></span>
	                </div>
                  </div>                 
            </div>
              <span class="text-danger"><?php echo form_error('username');?></span>
            <div class="input-group mb-3">
              <input  type="email" name="email"class="form-control  <?php echo (form_error('email') != "") ? 'is-invalid' : ''; ?>" placeholder="Email">
             
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-envelope"></span>
                </div> 
              </div>
            </div>
             <span class="text-danger"><?php echo form_error('email');?></span>
	        <div class="input-group mb-3">
              <input  type="password" name="password" class="form-control  <?php echo (form_error('password') != "") ? 'is-invalid' : ''; ?>" placeholder="Mot de passe">
             
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-lock"></span>
                </div>
              </div>
            </div>
             <span class="text-danger"><?php echo form_error('password');?></span>
	   	    <div class="input-group mb-3">
              <input type="password" name="password2" class="form-control  <?php echo (form_error('password') != "") ? 'is-invalid' : ''; ?>" placeholder="Retapez mot de passe">
             
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-lock"></span>
                </div>
              </div>
            </div>
              <span class="text-danger"><?php echo form_error('password2');?></span>
	   	    <div class="input-group mb-3">
              <select type="password" name="gender" class="form-control" placeholder="Genre">
	             <option value="Homme">Homme</option>
		       	 <option value="Femme">Femme</option> 
		       </select>
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-user"></span>
                </div>
              </div>
            </div>
	        <div class="input-group mb-3">
              <input type="text" name="phone"class="form-control  <?php echo (form_error('phone') != "") ? 'is-invalid' : ''; ?>" placeholder="Entre numero telephone">
             
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-phone"></span>
                </div>
              </div>
            </div>
             <span class="text-danger"><?php echo form_error('phone');?></span>
	         <div class="row">
              
              <div class="col-4">
                <button  name="register" class="btn btn-outline-success ">S'inscrire</button>
              </div>
              <!-- /.col -->

              </div>   
            </form>	   
	        <div class="social-auth-links text-center mb-3">
             <p>- OU -</p>
            
            </div>
	          <p class="mb-0">
	           <a href="<?php echo base_url(); ?>auth/index" class="text-center">Connectez-vous</a>
	          </p>
	   	   </div> 
    </div>
  </div>


  </div>
</body>
</html>