<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Impression commande</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap 4 -->

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url()."assets/";?>plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url()."assets/";?>dist/css/adminlte.min.css">

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body>
<div class="wrapper">
  <!-- Main content -->
  
    <br><section class="invoice" style="background: #fff;border: 1px solid rgba(0,0,0,.125);position: absolute;width: 90%;left: 5%;">
      <!-- title row -->

      <div class="row">
        <div class="col-12">
          <h4>
            <i class="fas fa-globe"></i> Gestion de vente d' arme
            <small class="float-right">Date: 13/03/2021</small>
          </h4>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          
          <address>
            <strong>Adresse vendeur</strong><br>
            E202, Andrainjato 306<br>
            Fianarantsoa,  94107<br>
            Tel: 034 25 079 77<br>
            Email: rakotonavalonaheritiana@gmail.com
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">

          <address>
           <?php 
            foreach ($fetch_data->result() as $row) {
           ?> 
            <strong>Info sur le client</strong><br>
            Nom: <?php echo $row->nom ;?><br>
            Adresse: <?php echo $row->adresse ;?><br>
            Tel: <?php echo $row->tel ;?><br>
         
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b>Info sur le paymant</b><br>
          <br>
          
          <b>ID:</b> #000<?php echo $row->id ;?><br>
           <?php }  ?> 
          <b>Date de paymant:</b>  13/03/2021<br>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-12 table-responsive">
         <table class="table table-striped">
            <thead>
            <tr>
              <th>ID</th>
              <th>Produit</th>
              <th>Prix</th>
              <th>Quantite</th>
              <th>Sous total</th>
            </tr>
            </thead>
            <tbody>
           <?php 
            foreach ($fetch_prod->result() as $row) {
           ?> 
            <tr>
              <td><?php echo $row->id; ?></td>
              <td><?php echo $row->designation; ?></td>
              <td><?php echo $row->prix; ?> Ar</td>
              <td><?php echo $row->qte; ?></td>
              <td><?php echo $row->qte * $row->prix; ?> Ar</td>
              
            </tr>
            <?php }  ?> 
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-6">
          
        </div>
        <!-- /.col -->
        <div class="col-6">
          <p class="lead">Total à payer du 2/22/2014</p>

          <div class="table-responsive">
            <table class="table">
              <tbody>

              <tr>
                <th>Total:</th>
                 <?php 
                    $s = 0;
                  foreach ($fetch_prod->result() as $row) {
                 ?> 
                  <?php
                  
                    $s = $s + ($row->qte * $row->prix); 
                  ?>
                 
                 <?php }  ?> 
                 <td> <?php echo $s; ?> Ar</td>
              </tr>
            </tbody></table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  </center>
  <!-- /.content -->
</div>
<!-- ./wrapper -->

<script type="text/javascript"> 
  window.addEventListener("load", window.print());
</script>
</body>
</html>
