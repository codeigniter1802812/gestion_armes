<?php $this->load->view('header');?>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Information sur les clients</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Accueil</a></li>
              <li class="breadcrumb-item active">Client</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
   
       <div class="row">
          <!-- Left col -->
          <div class="col-md-12">

             <?php if(isset($_SESSION['error'])){ ?> 
             <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-check"></i> Info! <?php echo $_SESSION['error']; ?></h5>
                 
             </div>
             <?php   } ?>
          </div>
        </div>
        <div class="row">
          <!-- Left col -->
          <div class="col-md-12">

             <?php if(isset($_SESSION['success'])){ ?> 
             <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-check"></i> Info! <?php echo $_SESSION['success']; ?></h5>
                 
             </div>
             <?php   } ?>
          </div>
        </div>
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <div class="col-md-8">

          
            <!-- TABLE: LATEST ORDERS -->
            <div class="card">
              <div class="card-header border-transparent">
                <h3 class="card-title"> <i class="fas fa-user"></i> Liste des clients</h3>

                <div class="card-tools">
                  <?php echo form_open('client/search');?>
                       <div class="input-group input-group-sm">
                       <input class="form-control form-control-navbar" placeholder="Chercher" aria-label="Search" name="key" id="key">
                        <div class="input-group-append">
                          <button class="btn btn-navbar" type="submit" name="submit">
                            <i class="fas fa-search"></i>
                          </button>
                        </div>
                    </div>
                  <?php echo form_close();?>
                 
                 
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <div class="table-responsive">
                  <table class="table m-0">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>Nom</th>
                      <th>Adresse</th>
                      <th>Tel</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php 
                    if($fetch_data->num_rows() > 0)
                    {	
                    	foreach ($fetch_data->result() as $row) {
                     ?> 		
                    	
	                    <tr>
	                      <td><?php echo $row->id; ?></td>
	                      <td><?php echo $row->nom; ?></td>   
	                      <td><?php echo $row->adresse; ?></td>  
	                      <td><?php echo $row->tel; ?></td>  
                        <td><a href="<?php echo base_url().'client/edit/'.$row->id?>" class="btn btn-warning btn-sm"><i class="far fa-edit"></i>Modifier</a>
                        <a href="javascript:void(0);" onclick="deteleClient(<?php echo $row->id; ?>)" class="btn btn-danger btn-sm"><i class="far fa-trash-alt"></i>Effacer</a></td>     
	                    </tr>
                    <?php
                      }
                    }
                    else 
                    {
                    ?>
                     <tr>
                     	<td>Aucun donnée trouver!!</td>
                     </tr>	
                    <?php } ?>
                    
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
            
               <?php
              
                $s = $fetch_data->num_rows();
               ?>
             
             
              <div class="card-footer clearfix">
                <h5 class="text-dark float-left" >
                  Total : <?php echo $s; ?> 
                </h5>
                <a href="<?php echo base_url(); ?>client/indexCli" class="btn btn-sm btn-secondary float-right">Toute client</a>
              </div>
       
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          
          <div class="col-md-4">
            <!-- Info Boxes Style 2 -->
             <div class="card card-default">
              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-info"></i> Ajout nouveau client</h3>
                
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>

              </div>
            <form action="" method="POST" name="myForm" onsubmit="return validateForm()"><br>
               
              <div class="card-body">
                <div class="form-group">
                  <label for="nom" class="text<?php echo (form_error('nom') != "") ? '-danger' : ''; ?>">Nom et prenom</label>
                  <input type="text" id="nom" name="nom" class="form-control <?php echo (form_error('nom') != "") ? 'is-invalid' : ''; ?>" value="<?php echo set_value('nom'); ?>">
                  <span class="text-danger"><?php echo form_error('nom');?></span>
                </div>
                <div class="form-group">
                  <label for="adresse" class="text<?php echo (form_error('adresse') != "") ? '-danger' : ''; ?>" >Adresse</label>
                  <input type="text" id="adresse" name="adresse" class="form-control <?php echo (form_error('adresse') != "") ? 'is-invalid' : ''; ?>" value="<?php echo set_value('adresse'); ?>">
                  <span class="text-danger"><?php echo form_error('adresse');?></span>
                </div>
                <div class="form-group">
                  <label for="tel" class="text<?php echo (form_error('tel') != "") ? '-danger' : ''; ?>">Telephone</label>
                  <input type="text" id="tel" value="<?php echo set_value('tel'); ?>" name="tel" class="form-control <?php echo (form_error('nom') != "") ? 'is-invalid' : ''; ?>">
                  <span class="text-danger"><?php echo form_error('tel');?></span>
                </div>
               
              </div>
              <div class="card-footer clearfix">
                  <input type="submit" value="Enregistrer" class="btn btn-success float-right">
              </div>
            </div>
           </form>
         
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
    
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
   <script>
            function validateForm() {
              
                var nom = document.forms["myForm"]["nom"].value;
                var adresse = document.forms["myForm"]["adresse"].value;
                var tel = document.forms["myForm"]["tel"].value;
               
            
                var maj = nom.toUpperCase();
                document.forms["myForm"]["nom"].value = maj;
                
                var maj = adresse.toUpperCase();
                document.forms["myForm"]["adresse"].value = maj;
                
                //tel
                if (isNaN(tel) == false) {
                    var isa = tel.length;
                    if (isa == 10) {
                        var cs = tel.substring(0, 3);
                        if (cs == "032" || cs == "033" || cs == "034" || cs == "039") {
                            document.forms["myForm"]["client.tel"].value = tel.substring(0, 3) + " " + tel.substring(3, 5) + " " + tel.substring(5, 8) + " " + tel.substring(8, 10);
                        } else {
                            alert("Operateur inconnu");
                            return false;
                        }
                    } else {
                        alert("Numero invalide");
                        return false;
                    }
                } else {
                    alert("Le numero doit être un chiffre");
                    return false;
                }
            }

        </script>
  <script type="text/javascript">
    function deteleClient(id){
       if (confirm("Voulez vouz supprimer?")) {
        window.location.href='<?php echo base_url().'client/delete/';?>'+id;
       }
    }
  </script>
<?php $this->load->view('footer');?>

